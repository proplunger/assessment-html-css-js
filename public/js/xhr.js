getProducts();
getSaveForLater();

var products;
var saves;
var saved;

function getProducts(){
    const xhr = new XMLHttpRequest();

    xhr.open('GET', 'http://localhost:3000/products');
    xhr.getResponseHeader("Content-Type", "application/json")

	xhr.send(null)

	xhr.onload = function () {
        console.log(xhr.response);
		products = JSON.parse(xhr.response);

		let str = "";

		products.forEach(function (product) {

			str += `<div class="card mb-3" style="max-width: 540px;">
			<div class="row g-0">
			  <div class="col-md-4">
				<img src="${product.thumbnail}" class="img-fluid rounded-start" alt="...">
			  </div>
			  <div class="col-md-8">
				<div class="card-body">
				  <h5 class="card-title">${product.title}</h5>
				  <p class="card-text">${product.description}</p>
				  <p class="card-text"><small class="text-muted">$${product.price}</small></p>
				  <button class="btn btn-primary" onclick="SaveProduct(${product.id})">Save For Later</button>
				</div>
			  </div>
			</div>
		  </div>`

		});

		document.getElementById("products").innerHTML = str;
}
}



function getSaveForLater() {

	const xhr = new XMLHttpRequest();

	xhr.open("GET", "http://localhost:3000/saveforLater");

	xhr.getResponseHeader("Content-Type", "application/json")

	xhr.send(null)

	xhr.onload = function () {

		saves = JSON.parse(xhr.responseText);

		let str = "";

		saves.forEach(function (save) {

			str += `<div class="card mb-3" style="max-width: 540px;">
			<div class="row g-0">
			  <div class="col-md-4">
				<img src="${save.thumbnail}" class="img-fluid rounded-start" alt="...">
			  </div>
			  <div class="col-md-8">
				<div class="card-body">
				  <h5 class="card-title">${save.title}</h5>
				  <p class="card-text">${save.description}</p>
				  <p class="card-text"><small class="text-muted">$${save.price}</small></p>
				  <button class="btn btn-danger" type="button" onclick="removeSaved(${save.id})">Remove From Saved</button>
				</div>
			  </div>
			</div>
		  </div>`

		});

		document.getElementById("saveforlater").innerHTML = str;

	}

}

var duplicate = false;

function SaveProduct(product_id) {

	products.forEach(function (product) {

		if (product.id === product_id) {

			saved = product;

		}

	});

	saves.forEach((save) => {

		if (save.id === product_id) {

			alert("Already Saved for Later.");

			duplicate = true;

		}

	});

	if (!duplicate) {

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "http://localhost:3000/saveforLater");
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.send(JSON.stringify(saved))

	}

}


// remove from saved For Later

function removeSaved(product_id) {

	var xhr = new XMLHttpRequest();
	xhr.open("DELETE", "http://localhost:3000/saveforLater/" + String(product_id));
	xhr.send(null)

}